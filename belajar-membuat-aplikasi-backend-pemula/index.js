// const intialMemoryUsage = process.memoryUsage().heapUsed
// const yourname = process.argv[2]
// const environtment = process.env.NODE_ENV

// for(let i= 0; i<=10000; i++) {

// }

// const currentMemoryUsage = process.memoryUsage().heapUsed

// console.log(`Hai ${yourname}`)
// console.log(`Mode environtment: ${environtment}`)
// console.log(`Penggunaan memori dari ${intialMemoryUsage} naik ke ${currentMemoryUsage}`)

// ---------------------------------------------

// const coffee = require('./lib/coffee')
// const user = require('./lib/user')

// console.log(coffee)
// console.log(user)

// ----------------------------------------------

// const moment = require('moment')

// const date = moment().format('MMM Do YY')
// console.log(date)

// ----------------------------------------------

// const { EventEmitter } = require('events')

// const myEventEmitter = new EventEmitter()

// const makeCoffee = (name) => {
//     console.log(`Kopi ${name} telah dibuat!`)
// }
// const makeBill = (price) => {
//     console.log(`Bill sebesar ${price} telah dibuat!`)
// }

// const onCofeeOrderedListener = ({name, price}) => {
//     makeCoffee(name)
//     makeBill(price)
// }

// myEventEmitter.on('coffee-order', onCofeeOrderedListener)

// myEventEmitter.emit('coffee-order', {name: 'Tubruk', price: 15000})

// ----------------------------------------------

// const fs = require('fs')

// const fileReadCallback = (error, data) => {
//     if (error) {
//         console.log('Gagal membaca berkas')
//         return
//     }
//     console.log(data)
// }

// // fs.readFile('todo.txt', 'UTF-8', fileReadCallback)
// console.log(fs.readFileSync('todo.txt', 'utf-8', fileReadCallback))

// ----------------------------------------------

// const fs = require('fs')

// const readableStream = fs.createReadStream('./article.txt', {
//     highWaterMark: 9
// })

// readableStream.on('readable', () => {
//     try {
//         process.stdout.write(`[${readableStream.read()}]`)
//     } catch(error) {
//         console.log(error)
//     }
// })

// readableStream.on('end', () => {
//     console.log('Done')
// })

// ----------------------------------------------

const fs = require('fs')

const writableStream = fs.createWriteStream('output.txt')

writableStream.write('Ini merupakan teks baris pertama!\n')
writableStream.write('Ini merupakan teks baris kedua!\n')
writableStream.end('Akhir dari writable stream!')