const fs = require('fs')
const path = require('path')

const folderInput = path.resolve(__dirname, 'input.txt')
const folderOutput = path.resolve(__dirname, 'output.txt')

const readableStream = fs.createReadStream(folderInput, {
    highWaterMark: 15
})

const writableStream = fs.createWriteStream(folderOutput)

readableStream.on('readable', () => {
    try {
        // process.stdout.read(`[${readableStream.read()}]`)
        // console.log(`${readableStream.read()}`)
        writableStream.write(`${readableStream.read()}\n`)
    } catch (err) {
        console.log(err)
    }
})

readableStream.on('end', () => {
    console.log('Done\nSuccessfully write!')
})